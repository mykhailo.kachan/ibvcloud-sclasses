public without sharing class sCase extends sObjectAbstract {

    private String getsObjectTypeAPIName() {
        return 'Case';
    }

    private List<String> getBaseFields() {
        return new List<String>{
                'Id',
                'Status',
                'AccountId'
        };
    }

    public List<Case> getRecordById(Id recordId) {
        System.debug(recordId);
        if(String.isBlank(recordId)){
            return new List<Case>();
        }
        return this.getRecordsByIds(new List<Id>{recordId}) ;
    }

    public List<Case> getRecordsByIds(List<Id> recordIds) {
        this.queryWhereConditions = this.createInCondition('Id', recordIds);
        return this.queryData();
    }
}